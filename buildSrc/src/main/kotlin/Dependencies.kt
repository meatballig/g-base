import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler

object Versions {
  const val kotlin = "1.6.0"
  const val gradle = "7.0.3"
  const val navigationSafeArgs = "2.3.5"

  const val buildTools = "30.0.3"
  const val compileSdk = 31
  const val targetSdk = 31
  const val minSdk = 23

  const val lifecycle = "2.4.0"
  const val adapterDelegates = "4.3.0"
  const val retrofit = "2.9.0"
  const val glide = "4.12.0"
  const val hilt = "2.40"
  const val navigation = "2.3.5"
  const val room = "2.3.0"

  const val mockk = "1.12.1"
}

object Dependencies {
  const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
  const val leakCanary = "com.squareup.leakcanary:leakcanary-android:2.7"
  const val timber = "com.jakewharton.timber:timber:4.7.1"
  const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.5.1"

  const val appcompat = "androidx.appcompat:appcompat:1.4.0"
  const val coreKtx = "androidx.core:core-ktx:1.7.0"
  const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.0.4"
  const val material = "com.google.android.material:material:1.4.0"
  const val fragment = "androidx.fragment:fragment-ktx:1.3.5"

  // Lifecycle
  const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
  const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycle}"
  const val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycle}"

  // Navigation
  const val navFragmentKtx = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
  const val navUiKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"

  // Adapter
  const val adapterDelegates =
    "com.hannesdorfmann:adapterdelegates4-kotlin-dsl:${Versions.adapterDelegates}"
  const val adapterDelegatesViewBinding =
    "com.hannesdorfmann:adapterdelegates4-kotlin-dsl-viewbinding:${Versions.adapterDelegates}"

  // Network
  const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
  const val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
  const val okHttpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:4.6.0"

  // Glide
  const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
  const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"

  // Hilt
  const val hilt = "com.google.dagger:hilt-android:${Versions.hilt}"
  const val hiltCompiler = "com.google.dagger:hilt-android-compiler:${Versions.hilt}"

  // Room
  const val room = "androidx.room:room-runtime:${Versions.room}"
  const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
}

object TestDependencies {
  const val junit = "junit:junit:4.13.2"
  const val truth = "com.google.truth:truth:1.1.3"
  const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.0-RC"
  const val mockk = "io.mockk:mockk:${Versions.mockk}"
  const val mockkJvm = "io.mockk:mockk-agent-jvm:${Versions.mockk}"
  const val coreTesting = "androidx.arch.core:core-testing:2.1.0"

  const val androidExtJUnit = "androidx.test.ext:junit:1.1.3"
  const val androidTestRunner = "androidx.test:runner:1.4.0"
  const val androidTestRules = "androidx.test:rules:1.4.0"
}

object BuildPlugins {
  const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
  const val gradle = "com.android.tools.build:gradle:${Versions.gradle}"
  const val navigationSafeArgs =
    "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigationSafeArgs}"
  const val hilt = "com.google.dagger:hilt-android-gradle-plugin:${Versions.hilt}"
}