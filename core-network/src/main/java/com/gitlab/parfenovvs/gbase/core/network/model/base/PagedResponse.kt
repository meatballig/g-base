package com.gitlab.parfenovvs.gbase.core.network.model.base

import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.google.gson.annotations.SerializedName

data class PagedResponse(
  @SerializedName("count") val count: Int,
  @SerializedName("next") val nextPageUrl: String,
  @SerializedName("previous") val previousPageUrl: String,
  @SerializedName("results") val results: List<GameDto>
)