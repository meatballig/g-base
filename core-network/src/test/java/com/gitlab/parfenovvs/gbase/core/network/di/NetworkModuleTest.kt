package com.gitlab.parfenovvs.gbase.core.network.di

import android.content.Context
import android.content.res.Resources
import io.mockk.every
import io.mockk.mockkClass
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import java.lang.RuntimeException

class NetworkModuleTest {

  private lateinit var mockContext: Context

  @Before
  fun setUp() {
    mockContext = mockkClass(Context::class)
  }

  @After
  fun tearDown() {
    unmockkAll()
  }

  @Test(expected = Test.None::class)
  fun provideApi_ApiKeyExists_ReturnsApi() {
    every { mockContext.getString(any()) } returns "key"
    NetworkModule.provideApi(mockContext)
  }

  @Test(expected = Resources.NotFoundException::class)
  fun provideApi_ApiKeyNonExists_ThrowsException() {
    every { mockContext.getString(any()) } throws Resources.NotFoundException()
    NetworkModule.provideApi(mockContext)
  }
}