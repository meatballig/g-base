package com.gitlab.parfenovvs.viewmodel.details

import androidx.lifecycle.SavedStateHandle
import com.gitlab.parfenovvs.ui.base.model.base.ListItem
import com.gitlab.parfenovvs.ui.base.model.game.ScreenshotItem
import com.gitlab.parfenovvs.model.GameDetailsModel
import com.gitlab.parfenovvs.util.require

sealed interface GameDetailsScreenModel {
  val id: Long
  val title: String
  val image: String?

  data class Initial(
    override val id: Long,
    override val title: String,
    override val image: String?,
  ) : GameDetailsScreenModel

  data class Content(
    override val id: Long,
    override val title: String,
    override val image: String?,
    val description: String,
    val screenshots: List<ListItem>,
  ) : GameDetailsScreenModel

  data class Error(
    override val id: Long,
    override val title: String,
    override val image: String?,
    val throwable: Throwable,
  ) : GameDetailsScreenModel

  companion object {
    fun from(handle: SavedStateHandle) = Initial(
      id = handle.require("gameId"),
      title = handle.require("gameTitle"),
      image = handle["gameImage"],
    )

    fun from(model: GameDetailsModel) = Content(
      id = model.id,
      title = model.title,
      image = model.image,
      description = model.description,
      screenshots = model.screenshots.map { ScreenshotItem(it) },
    )

    fun from(initial: Initial, throwable: Throwable) = Error(
      id = initial.id,
      title = initial.title,
      image = initial.image,
      throwable = throwable,
    )
  }
}