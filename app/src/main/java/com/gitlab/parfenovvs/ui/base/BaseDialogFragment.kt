package com.gitlab.parfenovvs.ui.base

import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import com.gitlab.parfenovvs.R
import com.gitlab.parfenovvs.util.launchAndRepeatOnStart
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.sample
import kotlinx.coroutines.launch

abstract class BaseDialogFragment : BottomSheetDialogFragment() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setStyle(DialogFragment.STYLE_NORMAL, R.style.BaseBottomSheetDialogTheme)
  }

  override fun onStart() {
    super.onStart()
    val behavior = BottomSheetBehavior.from(requireView().parent as View)
    behavior.skipCollapsed = true
    if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
      behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }
  }

  fun <T> Flow<T>.collectWhileStarted(block: (T) -> Unit) {
    launchAndRepeatOnStart {
      collect { block.invoke(it) }
    }
  }

  fun Flow<Throwable>.connectErrorData() {
    lifecycleScope.launch {
      sample(1000L).collect {
        if (lifecycle.currentState >= Lifecycle.State.STARTED) {
          Toast.makeText(context, R.string.default_error, Toast.LENGTH_SHORT).show()
        }
      }
    }
  }
}