package com.gitlab.parfenovvs.ui.base.model.game

import com.gitlab.parfenovvs.ui.base.model.base.ListItem

object ProgressThinItem : ListItem {
  override val itemId: Long = 0
}