package com.gitlab.parfenovvs.model


data class GameDetailsModel(
  val id: Long,
  val title: String,
  val image: String?,
  val description: String,
  val screenshots: List<String>,
)