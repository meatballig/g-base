package com.gitlab.parfenovvs.util

import androidx.annotation.StringRes

/**
 * Can be used for getting resources
 * Should be changed to support different themes
 */
interface ResourceProvider {
  fun string(@StringRes id: Int): String
}