package com.gitlab.parfenovvs.util

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.view.Display
import android.view.WindowManager


//TODO fix deprecation in API 30
fun getScreenSize(context: Context): Point {
  val display = (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
  val size = Point()
  display.getSize(size)
  return size
}