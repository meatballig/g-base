package com.gitlab.parfenovvs.interactor.main

import com.gitlab.parfenovvs.gbase.core.network.api.PagingState
import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.gitlab.parfenovvs.model.GameCategoryModel
import com.gitlab.parfenovvs.ui.base.model.game.*

object GameCategoryMappers {

  val INITIAL_PROGRESS_RANGE = IntRange(1, 3)

  fun mapToCategory(model: GameCategoryModel, wide: Boolean = false): GamesHorizontalItem =
    when (model.dataState) {
      is PagingState.Initial -> {
        GamesHorizontalItem(
          title = model.title,
          category = model.category,
          games = INITIAL_PROGRESS_RANGE
            .map { if (wide) ProgressWideItem else ProgressThinItem }
        )
      }
      is PagingState.Content -> {
        GamesHorizontalItem(
          title = model.title,
          category = model.category,
          games = model.dataState.data.toItems(wide)
        )
      }
      is PagingState.Paging -> {
        GamesHorizontalItem(
          title = model.title,
          category = model.category,
          games = model.dataState.availableContent.toItems(wide)
            .plus(if (wide) ProgressWideItem else ProgressThinItem)
        )
      }
      is PagingState.Persist -> {
        GamesHorizontalItem(
          title = model.title,
          category = model.category,
          games = model.dataState.data.toItems(wide)
            .plus(if (wide) ErrorWideItem(model.category) else ErrorThinItem(model.category))
        )
      }
      is PagingState.Error -> {
        GamesHorizontalItem(
          title = model.title,
          category = model.category,
          games = listOf(if (wide) ErrorWideItem(model.category) else ErrorThinItem(model.category))
        )
      }
    }

  private fun List<GameDto>.toItems(wide: Boolean) = map {
    if (wide) {
      GameWideItem(
        id = it.id,
        title = it.title,
        image = it.image
      )
    } else {
      GameThinItem(
        id = it.id,
        title = it.title,
        image = it.image
      )
    }
  }
}