package com.gitlab.parfenovvs.interactor.details

import com.gitlab.parfenovvs.model.GameDetailsModel
import com.gitlab.parfenovvs.repository.details.GameDetailsRepository
import com.gitlab.parfenovvs.repository.details.GameDetailsRepositoryImpl
import com.gitlab.parfenovvs.viewmodel.details.GameDetailsScreenModel
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.mockkClass
import io.mockk.unmockkAll
import kotlinx.coroutines.test.runTest

import org.junit.After
import org.junit.Before
import org.junit.Test

class GameDetailsInteractorImplTest {

  private lateinit var mockRepository: GameDetailsRepository
  private lateinit var interactor: GameDetailsInteractor

  @Before
  fun setUp() {
    mockRepository = mockkClass(GameDetailsRepositoryImpl::class)
  }

  @After
  fun tearDown() {
    unmockkAll()
  }

  @Test
  fun data_Success_ReturnsContentType() = runTest {
    val initial = GameDetailsScreenModel.Initial(1L, "title", "imageUrl")
    val gameDetailsModel =
      GameDetailsModel(initial.id, initial.title, initial.image, "", emptyList())

    interactor = GameDetailsInteractorImpl(initial, mockRepository)
    coEvery { mockRepository.data(initial.id) } returns gameDetailsModel

    val actualResult = interactor.data()

    assertThat(actualResult).isInstanceOf(GameDetailsScreenModel.Content::class.java)
  }

  @Test
  fun data_Success_ReturnsCorrectGame() = runTest {
    val initial = GameDetailsScreenModel.Initial(1L, "title", "imageUrl")
    val gameDetailsModel =
      GameDetailsModel(initial.id, initial.title, initial.image, "", emptyList())
    val expectedResult = GameDetailsScreenModel.from(gameDetailsModel)

    interactor = GameDetailsInteractorImpl(initial, mockRepository)
    coEvery { mockRepository.data(initial.id) } returns gameDetailsModel

    val actualResult = interactor.data() as GameDetailsScreenModel.Content

    assertThat(actualResult).isEqualTo(expectedResult)
  }

  @Test
  fun data_Fail_ReturnsErrorType() = runTest {
    val initial = mockkClass(GameDetailsScreenModel.Initial::class, relaxed = true)

    interactor = GameDetailsInteractorImpl(initial, mockRepository)
    coEvery { mockRepository.data(any()) } throws Exception()

    val actualResult = interactor.data()

    assertThat(actualResult).isInstanceOf(GameDetailsScreenModel.Error::class.java)
  }

  @Test
  fun data_Fail_ReturnsCorrectError() = runTest {
    val initial = GameDetailsScreenModel.Initial(1L, "title", "imageUrl")
    val exception = Exception()
    val expectedResult = GameDetailsScreenModel.from(initial, exception)

    interactor = GameDetailsInteractorImpl(initial, mockRepository)
    coEvery { mockRepository.data(initial.id) } throws exception

    val actualResult = interactor.data() as GameDetailsScreenModel.Error

    assertThat(actualResult).isEqualTo(expectedResult)
  }
}