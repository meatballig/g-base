# G-base (Unofficial [rawg.io](https://rawg.io) client)

This project was created for educational purposes.

Work on the G-Base project is ongoing.

## How to run

1. Get your free API key [here](https://rawg.io/apidocs).
2. Add `api_key` variable to your `local.properties` file as `api_key=YOUR_API_KEY`.
3. Sync project and run.

## Features

### Games lists

Master screen contains three horizontal lists with games are loaded from API.

##### Use-cases

- Animated progress placeholders
- Pagination
- Error item with "Try again" button
- Tap to show details
- Offline mode support

##### Screenshots

<img width="30%" src="image/screenshot1.png">
<img width="30%" src="image/screenshot2.png">

### Game details

Details screen is a simple screen to present more data about selected game.

##### Use-cases

- Reusing already loaded data from previous screen
- Showing cover, title, HTML description and screenshots
- Showing error state if the details are not able to be loaded

##### Screenshots

<img width="30%" src="image/screenshot3.png">
<img width="30%" src="image/screenshot4.png">
<img width="30%" src="image/screenshot5.png">

## Notes

### Memory leaks management

For this purpose [LeakCanary](https://square.github.io/leakcanary/) is included in **`leakManage`** build type.

### Libraries

All the third-party dependencies are in [Dependencies.kt](buildSrc/src/main/kotlin/Dependencies.kt).
